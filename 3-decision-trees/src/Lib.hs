{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: decision trees
    Copyright   : (c) Nick Roumimper, 2021
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum schrijven we een algoritme om de decision tree te bouwen voor een gegeven dataset.
    Meer informatie over het principe achter decision trees is te vinden in de stof van zowel DEP als CM.
    Let op dat we hier naar een simpele implementatie van decision trees toewerken; parameters zoals maximale
    diepte laten we hier expres weg. Deze code blijft splitsen tot er geen verbetering meer mogelijk is.
    De resulterende boom zal dus "overfit" zijn, maar dat is de verwachting.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.List (group, sort)

-- Allereerst definiëren we een datatype voor één enkele rij uit onze traindataset. (CRecord, voor Classification Record.)
-- Een rij uit onze dataset bestaat uit 
--     1) een lijst van numerieke eigenschappen van onze meeteenheid (properties);
--     2) een label voor de klasse waar deze meeteenheid toe behoort (label).
-- Bij het bouwen van onze boom weten we ook het label; ons doel is voor nieuwe data om op basis van de properties
-- te voorspellen welk label erbij hoort - maar dat implementeren we pas helemaal aan het eind.

data CRecord = CRecord { properties :: [Float]
                       , label :: String
                       }
  deriving (Show, Eq)

-- Onze dataset (CDataset, voor Classification Dataset) is dus simpelweg een lijst van CRecords.

type CDataset = [CRecord]

-- Bijgevoegd een paar simpele datasets, die je kunt gebruiken om mee te testen.

miniSet1 :: CDataset
miniSet1 = [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"]

miniSet2 :: CDataset
miniSet2 = [CRecord [1,1] "pink", CRecord [2,2] "pink", CRecord [3,3] "purple", CRecord [4,4] "blue", CRecord [5,5] "blue"]

miniSet3 :: CDataset
miniSet3 = [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green", CRecord [2,2] "green", CRecord [3,1] "orange", CRecord [3,2] "orange"] 



{- | 
    Deze functie haalt labels op uit een CDataset.

    Voorbeeld = getLabels [(1, "A"), (2, "B"), (3, "C")] ~> ["A", "B", "C"]
-}
getLabels :: CDataset -> [String]
-- | We gebruiken map om de label-functie toe te passen op elk element in de dataset en de labels te halen.
getLabels dataset = map label dataset


{- | 
    Deze functie berekent de frequentie van elke label.

    Voorbeeld = countLabels [(1, "A"), (2, "B"), (3, "A"), (4, "C"), (5, "B")] ~> [("A", 2), ("B", 2), ("C", 1)]
-}
countLabels :: CDataset -> [(String, Int)]
-- | We gebruiken getLabels om de lijst met labels te krijgen, sorteren deze, groeperen ze en tellen het aantal voorkomens.
countLabels dataset = map (\x -> (head x, length x)) (group (sort (getLabels dataset)))



{- | 
    Deze functie vindt het meest voorkomende label in een CDataset.

    Voorbeeld = mostFrequentLabel [(1, "A"), (2, "B"), (3, "A"), (4, "C"), (5, "B")] ~> "A"
-}

mostFrequentLabel :: CDataset -> String
-- | We gebruiken countLabels om een lijst van paren (label, count) te krijgen en vouwen deze lijst samen om het meest voorkomende label te vinden.
mostFrequentLabel dataset = fst (foldr1 (\(label1, count1) (label2, count2) -> if count1 >= count2 then (label1, count1) else (label2, count2)) (countLabels dataset))


-- We definiëren de volgende hulpfunctie (fd, voor "Float Division") om twee Ints te delen als twee Floats.
-- Voorbeeld: fd 3 4 ~> 0.75 (een Float), i.p.v. 0 (een Int).
fd :: Int -> Int -> Float
fd x y = (/) (fromIntegral x) (fromIntegral y)


{- | 
    Deze functie berekent de Gini-impuriteit van een CDataset.

    Voorbeeld = gini [(1, "A"), (2, "B"), (3, "A"), (4, "C"), (5, "B")] ~> 0.6399999999999997
-}

gini :: CDataset -> Float
-- | We gebruiken de formule voor Gini-impuriteit waarbij we de frequenties van de labels berekenen en deze gebruiken om de impuriteit te berekenen.
gini dataset = 1 - sum [fd (count) (length dataset) ^ 2 | (_, count) <- countLabels dataset]


{- | 
    Deze functie berekent de gewogen gemiddelde Gini-impuriteit na een split van een CDataset in twee subsets.

    Voorbeeld = 
    datas1 = [(1, "A"), (2, "B"), (3, "A")]
    datas2 = [(4, "C"), (5, "B")]
    giniAfterSplit datas1 datas2 ~> 0.4444444444444445
-}
giniAfterSplit :: CDataset -> CDataset -> Float
-- | Hier berekenen we de Gini-impuriteit voor beide subsets en wegen ze op basis van hun grootte in de dataset.
giniAfterSplit datas1 datas2 = gini datas1 * fd (length datas1) (length datas1 + length datas2) + gini datas2 * fd (length datas2) (length datas1 + length datas2)


-- ..:: Sectie 2 - Het genereren van alle mogelijke splitsingen in de dataset ::..
-- Bij het genereren van onze decision tree kiezen we telkens de best mogelijke splitsing in de data.
-- In deze simpele implementatie doen we dat brute force: we genereren alle mogelijke splitsingen
-- in de data, en checken ze allemaal. Hier beginnen we door de splitsingen te genereren.

-- We slaan elke mogelijke splitsing op in het datatype CSplit (voor Classification Split). Deze bestaat uit:
--     1) de eigenschap waarop gesplitst wordt, opgeslagen als de index in de lijst van properties (feature);
--     2) de waarde van deze feature waarop we splitsen - ofwel kleiner-gelijk-aan, ofwel groter dan (value).
-- Let op: feature refereert aan de positie in de lijst van properties van een CRecord.
-- Oftewel: als we het hebben over feature 1 van CRecord [8.0, 5.0, 3.0] "x", bedoelen we 5.0.
data CSplit = CSplit { feature :: Int
                     , value :: Float   
                     }
    deriving (Show, Eq, Ord)

{- | 
    Deze functie haalt de waarden van een bepaalde functie-index op uit een CDataset.

    Voorbeeld = 
    dataset = [(CRecord [1.0, 2.0, 3.0] "A"), (CRecord [4.0, 5.0, 6.0] "B")]
    getFeature 1 dataset ~> [2.0, 5.0]
-}
getFeature :: Int -> CDataset -> [Float]
-- | Hier gebruiken we map om de waarden van de opgegeven functie-index op te halen uit elk CRecord in de dataset.
getFeature featureIndex dataset = map (\(CRecord properties _) -> properties !! featureIndex) dataset


{- | 
    Deze functie haalt unieke waarden op uit een lijst van floating-point getallen en returnt ze in gesorteerde volgorde.

    Voorbeeld = 
    values = [3.0, 1.0, 2.0, 3.0, 1.0]
    getUniqueValuesSorted values ~> [1.0, 2.0, 3.0]
-}
getUniqueValuesSorted :: [Float] -> [Float]
-- | We sorteren de waarden, groeperen gelijke waarden en halen de unieke waarden eruit.
getUniqueValuesSorted = map head . group . sort


{- | 
    Deze functie berekent het gemiddelde van aangrenzende waarden in een lijst van float getallen.

    Voorbeeld =
    values = [1.0, 2.0, 3.0, 4.0]
    getAverageValues values ~> [1.5, 2.5, 3.5]
-}
getAverageValues :: [Float] -> [Float]
-- | Hier berekenen we het gemiddelde van aangrenzende waarden in de lijst.
getAverageValues [] = []
getAverageValues [_] = [] 
getAverageValues (x:y:xs) = ((x + y) / 2.0) : getAverageValues (y:xs)


{- | 
    Deze functie genereert een lijst van splitsingen voor een specifieke functie-index in een CDataset.

    Voorbeeld =
    dataset = [(CRecord [1.0, 2.0, 3.0] "A"), (CRecord [4.0, 5.0, 6.0] "B")]
    getFeatureSplits 0 dataset ~> [CSplit 0 2.5]
-}
getFeatureSplits :: Int -> CDataset -> [CSplit]
-- | We genereren splitsingen door de gemiddelde waarden van de unieke waarden van de opgegeven functie-index te nemen.
getFeatureSplits int dataset = [CSplit int splitValue | splitValue <- getAverageValues (getUniqueValuesSorted (getFeature int dataset))]


{- | 
    Deze functie genereert een lijst van alle splitsingen voor elke functie in een CDataset.

    Voorbeeld =
    dataset = [(CRecord [1.0, 2.0, 3.0] "A"), (CRecord [4.0, 5.0, 6.0] "B")]
    getAllFeatureSplits dataset ~> [CSplit 0 2.5, CSplit 1 3.5, CSplit 2 4.5]
-}
getAllFeatureSplits :: CDataset -> [CSplit]
-- | We genereren splitsingen voor elke functie-index van 0 tot het aantal functies in de dataset min 1.
getAllFeatureSplits dataset = concatMap (\featureIndex -> getFeatureSplits featureIndex dataset) [0..numFeatures - 1]
  where
    numFeatures = case dataset of
      [] -> 0
      (record:_) -> length (properties record)


-- ..:: Sectie 3 - Het vinden van de beste splitsing ::..
-- Nu we alle splitsingen hebben gegenereerd, rest ons nog de taak de best mogelijke te vinden.
-- Hiervoor moeten we eerst de functies schrijven om één CDataset, op basis van een CSplit,
-- te splitsen in twee CDatasets.

{- | 
    Deze functie bepaalt of een enkel CRecord wordt gesplitst op basis van een CSplit.

    Voorbeeld = 
    split = CSplit 1 3.0
    record = CRecord [2.0, 4.0, 1.0] "A"
    splitSingleRecord split record ~> True
-}
splitSingleRecord :: CSplit -> CRecord -> Bool
-- | We vergelijken de waarde van de eigenschap op de opgegeven functie-index met de splitswaarde en retourneren True als deze kleiner is, anders False.
splitSingleRecord (CSplit feature value) (CRecord properties _) = if properties !! feature <= value then True else False


{- | 
    Deze functie splitst een CDataset in twee subsets op basis van een CSplit.

    Voorbeeld =
    dataset = [(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B"), (CRecord [5.0, 6.0] "C")]
    split = CSplit 0 3.0
    splitOnFeature dataset split ~> ([(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B")], [(CRecord [5.0, 6.0] "C")])
-}
splitOnFeature :: CDataset -> CSplit -> (CDataset, CDataset)
-- | We gebruiken de splitswaarde en functie-index om de dataset te splitsen in twee subsets.
splitOnFeature dataset (CSplit feature value) = (filter (\(CRecord properties _) -> properties !! feature <= value) dataset, filter (\(CRecord properties _) -> properties !! feature > value) dataset)


{- | 
    Deze functie genereert alle mogelijke splitsingen voor een CDataset en de bijbehorende subsets.

    Voorbeeld = 
    dataset = [(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B"), (CRecord [5.0, 6.0] "C")]
    generateAllSplits dataset ~>
    [ (CSplit 0 3.0, [(CRecord [1.0, 2.0] "A")], [(CRecord [3.0, 4.0] "B"), (CRecord [5.0, 6.0] "C")]),
      (CSplit 0 4.0, [(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B")], [(CRecord [5.0, 6.0] "C")])    ]

-}
generateAllSplits :: CDataset -> [(CSplit, CDataset, CDataset)]
-- | We genereren alle mogelijke splitsingen en de bijbehorende subsets.
generateAllSplits dataset = [(split, leftDataset, rightDataset) | split <- allSplits]
  where
    allSplits = getAllFeatureSplits dataset
    (leftDataset, rightDataset) = splitOnFeature dataset (head allSplits)


{- | 
    Deze functie vindt de beste splitsing voor een CDataset op basis van de laagste Gini-impuriteit.

    Voorbeeld =
    dataset = [(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B"), (CRecord [5.0, 6.0] "C")]
    findBestSplit dataset ~> (0.0, CSplit 0 1.5)

-}
findBestSplit :: CDataset -> (Float, CSplit)
-- | We genereren alle mogelijke splitsingen en berekenen de Gini-impuriteit voor elke splitsing, en retourneren de splitsing met de laagste Gini-impuriteit.
findBestSplit dataset = foldr1 (\pair1 pair2 -> if fst pair1 < fst pair2 then pair1 else pair2) [(giniAfterSplit leftDataset rightDataset, split) | (split, leftDataset, rightDataset) <- generateAllSplits dataset]


-- ..:: Sectie 4 - Genereren van de decision tree en voorspellen ::..
-- In deze laatste sectie combineren we alle voorgaande om de decision tree op te bouwen,
-- en deze te gebruiken voor voorspellingen.

-- We introduceren het datatype van onze boom, de DTree (Decision Tree).
-- In de DTree is sprake van twee opties:
--     1) We hebben een blad van de boom bereikt, waarin we een voorspelling doen van het label (Leaf String);
--     2) We splitsen op een bepaalde eigenschap, met twee sub-bomen voor <= en > (Branch CSplit DTree DTree).
-- Zoals je al ziet is de definitie van Branch CSplit DTree DTree recursief; er kan dus een onbepaald aantal
-- vertakkingen zijn, maar uiteindelijk eindigt elke vertakking in een blad (Leaf).
-- Let op: we onthouden niet de records uit de dataset, maar wel waarop we ze gesplitst hebben (CSplit)!
data DTree = Branch CSplit DTree DTree | Leaf String deriving (Show, Eq, Ord)

{- | 
    Deze functie bouwt een decisiontree op basis van een CDataset.

    Voorbeeld =
    dataset = [(CRecord [1.0, 2.0] "A"), (CRecord [3.0, 4.0] "B"), (CRecord [5.0, 6.0] "C")]
    buildDecisionTree dataset ~> Branch (CSplit 0 3.5) (Leaf "A") (Branch (CSplit 1 5.0) (Leaf "B") (Leaf "C"))

-}
buildDecisionTree :: CDataset -> DTree
-- | We bouwen de decisiontree op basis van de Gini-impuriteit en splitsingen.
buildDecisionTree dataset
  | gini dataset == 0.0 || null dataset = Leaf (mostFrequentLabel dataset)
  | otherwise = Branch bestSplit (buildDecisionTree leftDataset) (buildDecisionTree rightDataset)
  where
    (bestGini, bestSplit) = findBestSplit dataset
    (leftDataset, rightDataset) = splitOnFeature dataset bestSplit



{- | 
    Deze functie voorspelt het label op basis van een decisiontree en de gegeven eigenschappen (properties).

    Voorbeeld =
    tree = Branch (CSplit 0 3.0) (Leaf "A") (Leaf "B")
    properties = [2.5, 4.0]
    predict tree properties ~> "A"

-}
predict :: DTree -> [Float] -> String
-- | We doorlopen de decisiontree op basis van de eigenschappen en voorspellen het label op basis van de splitsingen.
predict (Leaf label) _ = label
predict (Branch split leftTree rightTree) properties = predict (if splitSingleRecord split (CRecord properties "") then leftTree else rightTree) properties




