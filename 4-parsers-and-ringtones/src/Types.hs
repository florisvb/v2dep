{-# LANGUAGE TypeApplications #-}

module Types where

import Data.Int (Int32)

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

{- |
   De zipWithL functie combineert twee lijsten door de gegeven functie toe te passen en zoveel mogelijk lijsten samen te voegen.
   
   Als een van de lijsten opraken, worden de overgebleven elementen van de andere lijst ongewijzigd in de uitvoer opgenomen.

   Voorbeeld = zipWithL (+) [1, 2, 3] [10, 20] ~> [11, 22, 3]   
-}

zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL _ [] _ = []  
zipWithL _ xs [] = xs
-- | Hier wordt de functie f toegepast op de eerste elementen van de lijsten, en de rest van de lijsten wordt recursief verwerkt.
zipWithL f (x:xs) (y:ys) = f x y : zipWithL f xs ys


{- |
   De zipWithR functie combineert twee lijsten door de gegeven functie toe te passen en zoveel mogelijk lijsten samen te voegen.
   
   Als een van de lijsten opraken, worden de overgebleven elementen van de andere lijst ongewijzigd in de uitvoer opgenomen.

   Voorbeeld = zipWithR (+) [1, 2, 3] [10, 20] ~> [11, 22, 3]  
-}

zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR _ _ [] = [] 
zipWithR _ [] ys = ys 
-- | Hier wordt de functie f toegepast op de eerste elementen van de lijsten, en de rest van de lijsten wordt recursief verwerkt
zipWithR f (x:xs) (y:ys) = f x y : zipWithR f xs ys

data Sound = FloatFrames [Float]
  deriving Show

floatSound :: [Float] -> Sound
floatSound = FloatFrames

instance Eq Sound where
  (FloatFrames xs) == (FloatFrames ys) = (all ((<  0.001) . abs) $ zipWith (-) xs ys) && (length xs == length ys)

instance Semigroup Sound where
-- | Voegt twee geluiden samen door ze in volgorde af te spelen
  (FloatFrames a) <> (FloatFrames b) = FloatFrames (a ++ b)

instance Monoid Sound where
-- | Leeg geluid (niks om af te spelen)
  mempty = FloatFrames []

{- |
   De (<+>) operator combineert twee geluiden door ze tegelijkertijd af te spelen met element-wise optelling.

   Voorbeeld = (FloatFrames [0.35, 0.45, 0.25]) <+> (FloatFrames [0.35, 0.4499, 0.2499, 0.2999, 0.2001, 0.3999])
   FloatFrames [0.7, 0.8999, 0.5, 0.5999, 0.4501, 0.6499]
-}
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y) = FloatFrames (zipWithL (+) x y)


floatToInt32 :: Float -> Int32
floatToInt32 x = fromIntegral $ round x

getAsInts :: Sound -> [Int32]
getAsInts (FloatFrames fs) = map (floatToInt32 . \x -> x * fromIntegral (div (maxBound @Int32 ) 2 )) fs

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

{- |
   De modifyInstrument functie combineert een Modifier met een Instrument door de Modifier toe te passen op het geluid gegenereerd door het Instrument.
   
   Dit resulteert in een aangepast Instrument dat het oorspronkelijke geluid heeft gewijzigd volgens de opgegeven Modifier.
-}
modifyInstrument :: Instrument -> Modifier -> Instrument
modifyInstrument (Instrument instrumentFunction) (Modifier modifierFunction) =
-- | Hier wordt de Modifier toegepast op het geluid gegenereerd door het Instrument.
-- | Dit geeft een aangepast Instrument dat klaar is voor gebruik.
  Instrument $ \hz time -> modifierFunction (instrumentFunction hz time)


{- |
   De arrange functie past een gegeven Instrument toe op de opgegeven frequentie en duur om een geluid te genereren.

   Voorbeeld = 
   myInstrument = instrument (\hz time -> FloatFrames [0.1, 0.2, 0.3])
   frequency = 440
   duration = 1     
   sound = arrange myInstrument frequency duration
   getAsInts sound ~> [8192, 16384, 24576]
   
   In dit voorbeeld wordt de arrange functie gebruikt om een geluid te genereren met behulp van het myInstrument op een frequentie van 440 Hz en een duur van 1 seconde.

   Dit stelt gebruikers in staat om geluiden te genereren op basis van specifieke parameters zoals frequentie en duur.
-}
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument instrumentFunction) frequency duration = FloatFrames $ instrumentFunction frequency duration
