module Parsers where

import Control.Monad.Trans.State (StateT(..), evalStateT, put, get)
import Control.Applicative
import Control.Monad.Trans.State (StateT(..), evalStateT, put, get)
import Data.Maybe (isJust, fromMaybe)
import Data.Char (toUpper)
import Control.Monad(mzero, mplus)
import Data.List (uncons)
import Types
import Instruments

type Parser = (StateT String Maybe)
type CharSet = [Char]

pCharSet :: CharSet -> Parser Char
pCharSet cs = do input <- uncons <$> get
                 case input of
                   Just (h, t) -> if h `elem` cs then put t >> return h else mzero
                   Nothing -> mzero


{- |
   De pComplementCharSet functie parset het eerste karakter in de inputstroom dat niet in de opgegeven karakterset voorkomt.

   Dit is handig wanneer je specifieke karakters wilt negeren en alleen geïnteresseerd bent in karakters die niet in de karakterset voorkomen.

   Voorbeeld = evalStateT (pComplementCharSet "aeiou") "Hello" ~> Just 'H'
   
   In dit voorbeeld parset de pComplementCharSet functie het eerste karakter in de inputstroom "Hello" dat niet voorkomt in de karakterset "aeiou", wat resulteert in 'H'.
   
   Dit is nuttig bij het verwerken van tekstuele gegevens waarin bepaalde karakters moeten worden genegeerd.
-}
pComplementCharSet :: CharSet -> Parser Char
pComplementCharSet cs = do input <- uncons <$> get
                           case input of
                             Just (h, t) -> if h `elem` cs then mzero else put t >> return h
                             Nothing -> mzero


{- |
   De pString functie parset een reeks opeenvolgende karakters in de inputstroom die overeenkomen met de opgegeven tekenreeks.

   Dit is handig wanneer je specifieke teksten of tekenreeksen in de inputstroom wilt herkennen en extraheren.

   Voorbeeld = evalStateT (pString "Hello") "Hello, World!" ~> Just "Hello"
   
   In dit voorbeeld parset de pString functie de tekenreeks "Hello" in de inputstroom "Hello, World!", wat resulteert in "Hello".
   
   Dit kan worden gebruikt om specifieke tekstpatronen te identificeren en te verwerken in de inputgegevens.
-}
pString :: String -> Parser String
pString [] = return []
pString (x:xs) = (:) <$> pCharSet [x] <*> pString xs


pOptional :: Parser a -> Parser (Maybe a)
pOptional p = Just <$> p <|> return Nothing 

pRepeatSepBy :: Parser a -> Parser b -> Parser [b]
pRepeatSepBy sep p = (:) <$> p <*> mplus (sep *> pRepeatSepBy sep p) (return [])

pEmpty :: Parser ()
pEmpty = return ()


{- |
   De pRepeat functie past de opgegeven parser herhaaldelijk toe om een lijst van resultaten te genereren.

   Deze functie maakt gebruik van de pRepeatSepBy functie om herhaling met optionele scheidingstekens mogelijk te maken.

   Voorbeeld = evalStateT (pRepeat (pString "abc")) "abcabcabc" ~> Just ["abc","abc","abc"]
   
   In dit voorbeeld past de pRepeat functie de parser (pString "abc") herhaaldelijk toe op de inputstroom "abcabcabc", wat resulteert in de lijst ["abc","abc","abc"].

   Dit is handig bij het extraheren van herhalende tekstpatronen uit de inputgegevens.
-}
pRepeat :: Parser a -> Parser [a]
pRepeat p = pRepeatSepBy pEmpty p


{- |
   De pNumber functie parset een geheel getal (integer) vanuit de inputstroom.

   Deze functie gebruikt de pRepeat functie om opeenvolgende cijfers in de inputstroom te herkennen en converteert deze naar een integerwaarde.

   Voorbeeld = evalStateT pNumber "12345" ~> Just 12345
   
   In dit voorbeeld parset de pNumber functie het gehele getal "12345" vanuit de inputstroom en returnt het als de integerwaarde 12345.

   Dit is handig bij het identificeren en extraheren van numerieke waarden uit de inputgegevens.
-} 
pNumber :: Parser Int
pNumber = read <$> pRepeat (pCharSet "0123456789")

pTone :: Parser Tone
pTone = do tone <- tRead . toUpper <$> pCharSet "abcdefg"
           sharp <- pOptional (pCharSet "#")
           if isJust sharp && tone `elem` [C,D,F,G,A]
             then return (succ tone)
             else return tone
  where tRead 'C' = C
        tRead 'D' = D
        tRead 'E' = E
        tRead 'F' = F
        tRead 'G' = G
        tRead 'A' = A
        tRead 'B' = B
        tRead _   = error "Invalid note"


{- |
   De pOctave functie parset een getal en zet het om naar een bijbehorende octaafwaarde.

   Deze functie gebruikt de pNumber parser om een getal te herkennen en converteert het naar een octaafwaarde op basis van de overeenkomstige numerieke waarden.

   Voorbeeld = evalStateT pOctave "4" ~> Just Four
   
   In dit voorbeeld parset de pOctave functie het getal "4" vanuit de inputstroom en returnt het als de octaafwaarde Four.

   Dit is handig bij het identificeren en converteren van octaven in muzikale notaties.
-}
pOctave :: Parser Octave
pOctave = do
      octave <- pNumber
      case octave of
        0 -> return Zero
        1 -> return One
        2 -> return Two
        3 -> return Three
        4 -> return Four
        5 -> return Five
        6 -> return Six
        7 -> return Seven
        8 -> return Eight
        _ -> mzero
    
pDuration :: Parser Duration
pDuration = do number <- pNumber
               case number of
                 1 -> return Full
                 2 -> return Half
                 4 -> return Quarter
                 8 -> return Eighth
                 16 -> return Sixteenth
                 32 -> return Thirtysecond
                 _ -> mzero

pPause :: Duration -> Parser Note
pPause d = do duration <- fromMaybe d <$> pOptional pDuration
              _ <- pCharSet "pP"
              return $ Pause duration

pNote :: Duration -> Octave -> Parser Note
pNote d o = do duration <- fromMaybe d <$> pOptional pDuration
               tone <- pTone
               dot <- pOptional (pCharSet ".")
               octave <- fromMaybe o <$> pOptional pOctave
               return $ Note tone octave (if isJust dot then Dotted duration else duration)

pComma :: Parser ()
pComma = () <$ do _ <- pCharSet ","
                  pOptional (pCharSet " ")

{- |
   De pHeader' functie parset de headerinformatie van een RTTL-string en returnt een tupel met de naam van het bestand, de duur, octaaf en beats per minuut (bpm).

   Deze functie gebruikt verschillende parsers om de relevante informatie te extraheren uit de invoerstroom van de RTTL-string. 
   De headerinformatie volgt een specifiek formaat, waarbij de duur wordt aangegeven door de notatie ":d=", de octaaf door ":o=", en de bpm door ":b=".

   De duur wordt vertaald naar een bijbehorende waarde uit het 'Duration' datatype (bijvoorbeeld 'Quarter' voor duur 4), 
   de octaaf wordt vertaald naar een bijbehorende waarde uit het 'Octave' datatype, en de bpm wordt omgezet naar een geheel getal.

   Voorbeeld = evalStateT pHeader "AbbaSOS:d=4, o=6, b=124:" ~> Just ("AbbaSOS", Quarter, Six, 124)
   
   In dit voorbeeld parset de pHeader functie de invoerstroom "AbbaSOS:d=4, o=6, b=124:" 
   en returnt de headerinformatie als een tupel met de bestandsnaam "AbbaSOS," een duur van 'Quarter,' een octaaf van 'Six,' en een bpm van 124.

   Deze functie is handig bij het verwerken van RTTL-strings en het omzetten van de headerinformatie naar bruikbare gegevens.
-}
pHeader :: Parser (String, Duration, Octave, Beats)
pHeader = do
-- | Parse de bestandsnaam door karakters te herhalen totdat een dubbele punt wordt bereikt.
  name <- pRepeat (pComplementCharSet ":")
  _ <- pString ":d="
-- | Parse de duur en vertaal deze naar de bijbehorende waarde uit het Duration datatype.
  duration <- pNumber
  _ <- pString ", o="
  octave <- pNumber
  _ <- pString ", b="
-- | Parse de bpm en behoud deze als een geheel getal.
  bpm <- pNumber
  _ <- pCharSet ":"
  let durationInBeats = case duration of
        1 -> Full
        2 -> Half
        4 -> Quarter
        8 -> Eighth
-- | Geef het resultaat als een tupel met de verwerkte informatie.
  return (name, durationInBeats, toOctave octave, fromIntegral bpm)
  where
-- | Functie om de numerieke octaafwaarde om te zetten naar de bijbehorende Octave waarde.
    toOctave 0 = Zero
    toOctave 1 = One
    toOctave 2 = Two
    toOctave 3 = Three
    toOctave 4 = Four
    toOctave 5 = Five
    toOctave 6 = Six
    toOctave 7 = Seven
    toOctave 8 = Eight



pSeparator :: Parser ()
pSeparator = () <$ foldl1 mplus [pString " ", pString ", ", pString ","]

pRTTL :: Parser (String, [Note], Beats)
pRTTL = do (t, d, o, b) <- pHeader
           notes <- pRepeatSepBy pSeparator $ mplus (pNote d o) (pPause d)
           return (t, notes, b)

parse :: String -> Maybe (String, [Note], Beats)
parse x = evalStateT pRTTL x