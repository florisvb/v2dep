{-|
    Module      : Lib
    Description : Checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl & Nick Roumimper, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata.
    Een cellulair automaton is een rekensysteem dat, gegeven een startsituatie en regels,
    in tijdstappen bepaalt of diens cellen (vakjes) "aan" of "uit" zijn ("levend" of "dood"). 
    Denk aan Conway's Game of Life [<https://playgameoflife.com/>], maar dan in één dimensie (op een lijn).
    Als we de tijdstappen verticaal onder elkaar plotten, krijgen we piramides met soms verrassend complexe patronen erin.
    LET OP: lees sowieso de informatie op [<https://mathworld.wolfram.com/Rule30.html>] en de aangesloten pagina's!
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)


-- ..:: Sectie 1: Basisoperaties op de FocusList ::..

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. 
-- Dit type representeert een (1-dimensionale) lijst, met een enkel element dat "in focus" is. 
-- Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.
-- Een voorbeeld van zo'n "gefocuste" lijst: 
--     [0, 1, 2, <3>, 4, 5]
-- waarbij 3 het getal is dat in focus is.
-- In Haskell representeren we de bovenstaande lijst als:
--     FocusList [3,4,5] [2,1,0]
-- waarbij het eerste element van de eerste lijst in focus is.
-- De elementen die normaal vóór de focus staan, staan in omgekeerde volgorde in de tweede lijst.
-- Hierdoor kunnen we makkelijk (lees: zonder veel iteraties te hoeven doen) bij de elementen rondom de focus,
-- en de focus makkelijk één plaats opschuiven.

data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

{- | 
    Functie maakt van een focuslist een gewone list
    Voorbeeld: toList intVoorbeeld ~> [0,1,2,3,4,5]
-}

toList :: FocusList a -> [a]
-- Voeg eerst de omgekeerde ys lijst toe aan het einde van xs om de juiste volgorde te behouden.
toList (FocusList xs ys) = reverse(ys) ++ xs


{- | 
    Converteert een gewone lijst naar een FocusList.
    Voorbeeld: Lijst [0,1,2,3,4,5] ~> FocusList [0,1,2,3,4,5][]
-}

fromList :: [a] -> FocusList a
-- De inputlijst xs wordt toegevoegd aan het eerste gedeelte van de FocusList,
-- en het tweede gedeelte wordt leeg gelaten omdat er geen focus-informatie is.
fromList xs = FocusList xs []

{- | 
    Schuift de focus met één naar links op.
    Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [2,3,4,5] [1,0]
-}

goLeft :: FocusList a -> FocusList a
-- De huidige focus wordt verplaatst van bw naar fw.
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

{- | 
    Schuift de focus met één naar rechts op.
    Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [4,5] [3,2,1,0]
-}
goRight :: FocusList a -> FocusList a
-- De huidige focus wordt verplaatst van fw naar bw.
goRight (FocusList (f:fw) bw) = FocusList fw (f:bw)

{- | 
    Schuift de focus helemaal naar links op.
    Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [0,1,2,3,4,5] []
-}
leftMost :: FocusList a -> FocusList a
-- Verplaats alle elementen in bw naar fw en maak bw leeg
leftMost (FocusList fw bw) = FocusList (reverse(bw) ++ fw) []

{- | 
    Schuift de focus helemaal naar rechts op.
    Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [][0,1,2,3,4,5]
-}
rightMost :: FocusList a -> FocusList a
-- Verplaats alle elementen in fw naar bw en maak fw leeg
rightMost (FocusList fw bw) = FocusList [last fw] (tail $ reverse(fw) ++ bw) 

-- Onze functies goLeft en goRight gaan er impliciet van uit dat er links respectievelijk rechts een waarde gedefinieerd is. 
-- De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen, omdat er in een lege lijst gezocht wordt: er is niets verder naar links. 
-- Dit is voor onze toepassing niet handig, omdat we vaak de cellen direct links en rechts van de focus 
-- nodig hebben, ook als die (nog) niet bestaan.

-- Schrijf de functies totalLeft en totalRight die de focus naar links respectievelijk rechts opschuift; 
-- als er links/rechts geen vakje meer is, dan wordt een lege (dode) cel teruggeven. 
-- Hiervoor gebruik je de waarde `mempty`, waar we met een later college nog op in zullen gaan. 
-- Kort gezegd zorgt dit ervoor dat de FocusList ook op andere types blijft werken - 
-- je kan dit testen door totalLeft/totalRight herhaaldelijk op de `voorbeeldString` aan te roepen, 
-- waar een leeg vakje een lege string zal zijn.
-- NOTE: deze functie werkt niet met intVoorbeeld! (Omdat mempty niet bestaat voor Ints - maar daar komen we nog op terug!)

-- Grafisch voorbeeld: [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚totalLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

-- TODO: Schrijf en documenteer de functie totalLeft, zoals hierboven beschreven.
{- | 
    totalLeft schuift de focus op naar links; 
    als er links geen vakje meer is, dan wordt een lege (dode) cel teruggeven. 
    Grafisch voorbeeld: [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚totalLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]
-}
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
-- Als bw leeg is, voeg een nieuw leeg element toe aan het begin van fw voordat de focus wordt verplaatst.
totalLeft (FocusList fw []) = FocusList (mempty:fw) []
-- Verplaats gewoon de focus naar links met behulp van de functie goLeft.
totalLeft (FocusList fw bw) = goLeft(FocusList fw bw)

{- | 
    totalRight schuift de focus op naar rechts; 
    als er rechts geen vakje meer is, dan wordt een lege (dode) cel teruggeven. 
    Grafisch voorbeeld: FocusList [⟨░⟩] [▓, ▓, ▓] ⤚totalRight→ [▓, ⟨░⟩] [▓, ▓, ▓]
-}
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
-- Als er maar één cel over is aan de linkerkant, voeg een nieuwe lege cel toe aan de rechterkant.
totalRight (FocusList [x] bw) = FocusList [mempty] (x:bw)
-- Verplaats gewoon de focus naar rechts met behulp van de functie goRight.
totalRight (FocusList fw bw) = goRight(FocusList fw bw)


-- ..:: Sectie 2 - Hogere-ordefuncties voor de FocusList ::..

-- In de colleges hebben we kennis gemaakt met een aantal hogere-orde functies zoals `map`, `zipWith` en `fold[r/l]`. 
-- Hier stellen we equivalente functies voor de FocusList op.


{- | 
    Deze functie accepteert een functie die wordt toegepast op elk element van de FocusList.
    Voorbeeld = [3,4,5] [2,1,0] mapFocusList (-1)~> FocusList [2,3,4][1,0,-1]
-}
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
-- Pas de functie toe op alle elementen in zowel 'fw' als 'bw' om de nieuwe lijsten te construeren.
mapFocusList functie (FocusList fw bw) = FocusList (map (functie) fw) (map (functie) bw)

-- TODO: Schrijf en documenteer de functie zipFocusListWith.
-- Deze functie zorgt ervoor dat ieder paar elementen uit de FocusLists als volgt met elkaar gecombineerd wordt:

-- [1, 2, ⟨3⟩,  4, 5]        invoer 1
-- [  -1, ⟨1⟩, -1, 1, -1]    invoer 2
--------------------------- (*)
-- [  -2, ⟨3⟩, -4, 5    ]    resultaat

-- of, in code: zipFocusListWith (*) (FocusList [3,4,5] [2,1]) (FocusList [1,-1,1,-1] [-1]) ~> FocusList [3,-4,5] [-2]
-- Oftewel: de meegegeven functie wordt aangeroepen op de twee focus-elementen, met als resultaat het nieuwe focus-element. 
-- Daarnaast wordt de functie paarsgewijs naar links/rechts doorgevoerd, waarbij gestopt wordt zodra een van beide uiteinden leeg is. 
-- Dit laatste is net als bij de gewone zipWith, die je hier ook voor mag gebruiken.

{- | 
    Deze functie accepteert een functie en twee FocusLists. Het past de functie element-wise toe
    Voorbeeld = Lijst1[3,4,5][2,1] Lijst2[2,1,0][3,4] zipFocusListWith (+)~> FocusList [5,5,5][5,5]
-}
zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
-- Combineer de elementen van fw en xs met de opgegeven functie en de elementen van bw en ys met de opgegeven functie.
zipFocusListWith functie (FocusList fw bw) (FocusList xs ys) = FocusList (zipWith (functie) fw xs) (zipWith (functie) bw ys)

{- | 
   Deze functie accepteert een functie en een FocusList. Het combineert alle elementen van de FocusList 
   met behulp van de functie en retourneert het resultaat van de combinatie.
   Voorbeeld = [1,2,3][4,5] foldFocusList (+)~> 15
-}

foldFocusList :: (a -> a -> a) -> FocusList a -> a
-- Als bw leeg is, gebruik foldr1 om de functie op fw toe te passen.
foldFocusList functie (FocusList fw []) = foldr1 functie fw
-- Als bw niet leeg is, gebruik foldr1 om de functie op de omgekeerde bw toe te passen
-- en foldl1 op fw en combineer de resultaten met de opgegeven binaire functie.
foldFocusList functie (FocusList fw bw) = (foldr1 functie (reverse bw)) `functie` (foldl1 functie fw)


-- Mocht je foldFocusList handmatig willen testen, dan kun je testFold opvragen via GHCi.
-- Deze geeft True als alles klopt, en False zo niet. Uiteraard kun je ook handmatig onderdelen hier uit halen.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld               == 15
               , foldFocusList (-) intVoorbeeld               == 7
               , foldFocusList (*) (FocusList [1,3,5,7,9] []) == 945
               , foldFocusList (++) stringVoorbeeld           == "012345"
               ]


-- ..:: Sectie 2.5: Types voor cellulaire automata ::..

-- Nu we een redelijk complete FocusList hebben, kunnen we deze gaan gebruiken om cellulaire automata in te ontwikkelen.
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een cel kan ofwel levend, ofwel dood zijn.
data Cell = Alive | Dead deriving (Show, Eq)

-- De onderstaande instance-declaraties mag je in dit practicum negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- De huidige status van ons cellulair automaton beschrijven we als een FocusList van Cells.
-- Dit type noemen we Automaton.
type Automaton = FocusList Cell

-- De standaard starttoestand bestaat uit één levende cel in focus.
start :: Automaton
start = FocusList [Alive] []

-- De context van een cel is de waarde van een cel, samen met de linker- en rechterwaarde, op volgorde.
-- Voorbeeld: de context van 4 in [1,2,3,4,5,6] is [3,4,5].
-- In de praktijk bestaat de context altijd uit drie cellen, maar dat zie je niet terug in dit type.
type Context = [Cell]

-- Een regel is een mapping van elke mogelijke context naar de "volgende state" - levend of dood.
-- Omdat er 2^3 = 8 mogelijke contexts zijn, zijn er 2^8 = 256 geldige regels.
-- Zie voor een voorbeeld [<https://mathworld.wolfram.com/Rule30.html>].
type Rule = Context -> Cell


-- ..:: Sectie 3: Rule30 en helperfuncties ::..

{- | 
   Deze functie accepteert een standaardwaarde en een lijst van elementen. Als de lijst leeg is, wordt de standaardwaarde geretourneerd.
   Als de lijst niet leeg is, wordt het eerste element van de lijst geretourneerd.
   Voorbeeld = 0 [1, 2, 3] safeHead ~> 1
-}
safeHead :: a        -- ^ Defaultwaarde
         -> [a]      -- ^ Bronlijst
         -> a
-- Als de lijst leeg is, retourneer de opgegeven standaardwaarde.
safeHead waarde [] = waarde
-- Als de lijst niet leeg is, retourneer het eerste element van de lijst.
safeHead _ (x:_) = x

{- | 
   Deze functie accepteert een minimum aantal items, een standaardwaarde en een lijst van elementen. 
   Het retourneert een nieuwe lijst met minimaal het opgegeven aantal items. 
   Als de lijst niet genoeg elementen bevat worden de ontbrekende items aangevuld met de standaardwaarde.
   Voorbeelden: takeAtLeast 4 0 [1,2,3,4,5] ~> [1,2,3,4]
                takeAtLeast 4 0 [1,2]       ~> [1,2,0,0]
-}

takeAtLeast :: Int   -- ^ Aantal items om te pakken
            -> a     -- ^ Defaultwaarde
            -> [a]   -- ^ Bronlijst
            -> [a]
-- Neem minimaal aantal items uit de lijst en vul eventuele ontbrekende items aan met de opgegeven waarde.
takeAtLeast aantal waarde lijst = take aantal (lijst ++ repeat waarde)

{- | 
   Bepaalt de context van een automaat op basis van een FocusList
   Voorbeeld: de context van 4 in [1,2,3,4,5,6] is [3,4,5]
-}
context :: Automaton -> Context
-- Neem eerst het eerste element van y en vervolgens eerste 2 van x, met toevoeging van 'Dead' als een lijst leeg is.
context (FocusList x y) = takeAtLeast 1 Dead y ++ takeAtLeast 2 Dead x

-- TODO: Schrijf en documenteer de functie expand die een Automaton uitbreidt met een dode cel aan beide uiteindes. 
-- We doen voor deze simulatie de aanname dat de "known universe" iedere ronde met 1 uitbreidt naar zowel links als rechts.
{- | 
   Deze functie accepteert een FocusList en breidt deze uit door aan het linkerkant een dode cel toe te voegen en aan de rechterkant
   Grafisch voorbeeld: FocusList [⟨░⟩] [▓, ▓, ▓] ⤚expand→ [░, ⟨░⟩] [▓, ▓, ▓, ░]
-}
expand :: Automaton -> Automaton
-- Voeg een dode cel toe aan het linkeruiteinde, keer de volgorde van x om en voeg een dode cel toe aan het rechteruiteinde.
expand (FocusList x y) = FocusList (reverse (Dead : x)) (y ++ [Dead])

{- |
   Deze functie accepteert een lijst van drie cellen en retourneert 
   de nieuwe waarde van het middelste element op basis van de Rule.
-}
rule30 :: Rule
-- Alle mogelijke combinaties van drie cellen.
rule30 [Dead, Dead, Dead] = Dead
rule30 [Dead, Dead, Alive] = Alive
rule30 [Dead, Alive, Dead] = Alive
rule30 [Dead, Alive, Alive] = Alive
rule30 [Alive, Dead, Dead] = Alive
rule30 [Alive, Dead, Alive] = Dead
rule30 [Alive, Alive, Dead] = Dead
rule30 [Alive, Alive, Alive] = Dead

-- Je kan je rule-30 functie in GHCi (voer `stack ghci` uit) testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start
-- (Lees sectie 3.5 voor uitleg over deze functies - en hoe het afdrukken nou precies werkt!)

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}


-- ..:: Sectie 3.5: Het herhaaldelijk uitvoeren en tonen van een cellulair automaton ::..
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een reeks van Automaton-states achtereen noemen we een TimeSeries. Effectief dus gewoon een lijst van Automatons over tijd.
type TimeSeries = [Automaton]

-- De functie iterateRule voert een regel n keer uit, gegeven een starttoestand (Automaton). 
-- Het resultaat is een reeks van Automaton-states.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s]
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s)
  where applyRule :: Automaton -> Context
        applyRule (FocusList [] bw) = []
        applyRule z = r (context z) : applyRule (goRight z)

-- De functie showPyramid zet een reeks van Automaton-states om in een String die kan worden afgedrukt.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"


-- ..:: Sectie 4: Cellulaire automata voor alle mogelijke regels ::..

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. 
-- Zoals op de voorgenoemde pagina te zien is, heeft het nummer te maken met binaire codering. 
-- De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). 
-- Afhankelijk van het nummer dat een regel heeft, mapt iedere combinatie naar een levende of dode cel.


inputs :: [Context]
-- Een lijst van verschillende contexten die worden gebruikt als invoer.
inputs =
    [ 
      [Alive, Alive, Alive]
    , [Alive, Alive, Dead]
    , [Alive, Dead, Alive]
    , [Alive, Dead, Dead]
    , [Dead, Alive, Alive]
    , [Dead, Alive, Dead]
    , [Dead, Dead, Alive]
    , [Dead, Dead, Dead]
    ]


-- Deze helperfunctie evalueert met de functie (p) de waarde (x); als dit True teruggeeft, is het resultaat Just x, anders Nothing. 
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing


binary :: Int -> [Bool]
  -- Stap 1: We willen de binaire representatie van de integer bepalen.
    -- We beginnen met de integer en blijven deze door 2 delen totdat we nul bereiken.
    -- Onderweg nemen we de rest van elke deling, die 0 of 1 zal zijn, en bewaren we deze.
    -- Dit proces genereert een lijst van paren waarvan het eerste element de rest is
    -- en het tweede element is wat overblijft om verder te delen.
    -- Bijvoorbeeld, voor n = 5 krijgen we [(1, 2), (0, 1)].
  -- Stap 2: We nemen alleen de eerste 8 paren uit de lijst.
  -- Stap 3: Als de lijst minder dan 8 paren bevat, voegen we aan het einde
    -- genoeg nullen (False) toe om hem op te vullen tot 8 elementen.
  -- Stap 4: We keren de lijst om om de binaire representatie in de juiste volgorde te krijgen.
  -- Stap 5: We converteren de lijst van integers naar een lijst van Booleans.
    -- Dit betekent dat 0 wordt omgezet naar False en 1 naar True.
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)


{- |
   Deze functie accepteert een lijst van booleans als masker en een lijst van elementen. Het retourneert een nieuwe lijst
   die bestaat uit de elementen uit de oorspronkelijke lijst waarvan de overeenkomstige boolean in het masker True is
   Voorbeeld = lijst_masker = [True,False,True,False,True] lijst_elementen = [1,2,3,4,5] Mask~> [1,3,5]
-}
mask :: [Bool] -> [a] -> [a]
-- Als het masker leeg is of de lijst leeg is, is het resultaat een lege lijst.
mask [] _ = []
mask _ [] = []
-- Als het masker True is op de huidige positie, voeg het overeenkomstige element toe aan het resultaat
-- anders ga verder met de volgende elementen.
mask (True:bs) (x:xs) = x : mask bs xs
mask (False:bs) (_:xs) = mask bs xs

{- |
   Deze functie zet elk getal om naar de bijbehorende regel. 
   De Int staat hierbij voor het nummer van de regel; de Context `input` is waarnaar je kijkt om te zien of het resultaat
   met de gevraagde regel Dead or Alive is. 
-}
rule :: Int -> Rule
-- Controleer of de context overeenkomt met de regel.
rule n context = if context `elem` mask (binary n) inputs 
                    -- Als de context overeenkomt return Alive
                    then Alive 
                    -- Anders return Dead
                    else Dead


{- Je kunt je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   Als het goed is zal `stack run` nu ook werken met de optie (d) uit het menu; 
   experimenteer met verschillende parameters en zie of dit werkt.
-}
