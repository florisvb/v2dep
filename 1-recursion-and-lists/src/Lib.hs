{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

{- | 
    Som van alle elementen van een lijst met recursie
    Voorbeeld: ex1 [3,1,4,1,5] ~> 14
-}


ex1 :: [Int] -> Int
-- | Dit is het geval wanneer de lijst leeg is, dan geven we 0 
ex1 []     = 0        
-- | Hier doen we het eerste element plus de som van de rest van de lijst.
ex1 (x:xs) = x + ex1 xs


{- | 
    Alle integers van een lijst doen we plus 1 door behulp van recursie
    Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
-}
ex2 :: [Int] -> [Int]
-- | Dit is het geval wanneer de lijst leeg is, we stoppen de loop door een lege lijst terug te geven.
ex2 [] = []
-- | Hier doen we alle elementen in de lijst plus 1.
ex2 (x:xs) = (x + 1) : ex2 xs


{- | 
    Alle integers van een lijst doen we keer  -1 door behulp van recursie
    Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
-}
ex3 :: [Int] -> [Int]
-- | Dit is het geval wanneer de lijst leeg is, we stoppen de loop door een lege lijst terug te geven.
ex3 []= []
-- | Hier doen we alle elementen in de lijst keer -1.
ex3 (x:xs) = (x * (-1)) : ex3 xs


{- |
    Hierbij plakken we 2 lijsten aan elkaar door middel van recursie
    Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-}
ex4 :: [Int] -> [Int] -> [Int]
-- Als de eerste lijst leeg is ([]), wordt de tweede lijst (xs) direct teruggegeven. 
-- Dit zorgt ervoor dat we de tweede lijst onveranderd behouden.
ex4 [] xs = xs
-- Voeg het eerste element van de eerste lijst toe aan het begin en ga door met de rest van de lijsten.
ex4 (x:xs) ys = x : ex4 xs ys


{-|
    Hierbij tellen we paarsgewijs twee lijsten bij elkaar op.
    Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
-}
ex5 :: [Int] -> [Int] -> [Int]
-- Als de eerste lijst leeg is, retourneren we de tweede lijst.
ex5 [] xs = xs
-- Als de tweede lijst leeg is, retourneren we de eerste lijst.
ex5 ys [] = ys
-- In het recursieve geval voegen we het eerste element van beide lijsten samen en gaan verder met de rest.
ex5 (x:xs) (y:ys) = (x + y) : ex5 xs ys


{-|
    Hierbij vermenigvuldigen we paarsgewijs twee lijsten met elkaar.
    Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36]
-}
ex6 :: [Int] -> [Int] -> [Int]
-- Als de eerste lijst leeg is, retourneren we de tweede lijst.
ex6 [] xs = xs
-- Als de tweede lijst leeg is, retourneren we de eerste lijst.
ex6 ys [] = ys
-- In het recursieve geval vermenigvuldigen we het eerste element van beide lijsten samen en gaan verder met de rest.
ex6 (x:xs) (y:ys) = (x * y) : ex6 xs ys


{- |
    Deze functie neemt twee lijsten van gehele getallen (xs en ys) als invoer 
    en berekent het inwendig product door zowel de functie ex1 als ex6 te gebruiken.
    Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.
-}
ex7 :: [Int] -> [Int] -> Int
-- Als invoer krijgen we twee lijsten (xs en ys). We passen eerst ex6 toe om het elementgewijze product van xs en ys te berekenen.
-- Vervolgens passen we ex1 toe op het resultaat om de som van deze producten te krijgen, wat het inwendig product is.
ex7 xs ys = ex1 (ex6 xs ys)


